//
//  EditBudgetingController.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 14/06/21.
//

import UIKit

protocol EditBudgetingDelegate {
    func refresh(budget: Budget)
}

class EditBudgetingController: UIViewController {
    @IBOutlet weak var buttonName: UIButton!
    @IBOutlet weak var buttonCategories: UIButton!
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    private var budget: Budget?
    var editBudgetingDelegate: EditBudgetingDelegate!
    private lazy var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonName.backgroundColor = .clear
        buttonName.layer.borderWidth = 2
        buttonName.layer.borderColor = Colors.yellow.cgColor
        buttonName.layer.cornerRadius = 20
        
        buttonCategories.backgroundColor = .clear
        buttonCategories.layer.borderWidth = 2
        buttonCategories.layer.borderColor = Colors.yellow.cgColor
        buttonCategories.layer.cornerRadius = 20
        
        if let data = self.budget,
           let image = data.imageData,
           let category = data.category {
            categoryImage.image = UIImage(data: image)
            categoryImage.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 25)
            nameLabel.text = category
        }
        
        imagePicker.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let data = budget {
            if let imageData = categoryImage.image?.pngData() {
                data.imageData = imageData
                
                DataManager.shared.saveData()
            }
            
            editBudgetingDelegate.refresh(budget: data)
        }
    }
    
    @IBAction func editImageTapped(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
}

// MARK: Image Picker
extension EditBudgetingController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            categoryImage.image = image
        }
        dismiss(animated: true, completion: nil)
    }
}

// MARK: Data Transmission
extension EditBudgetingController {
    static let showCategories = "showCategories"
    static let editCategory = "editCategoryName"
    
    func configure(with budget: Budget) {
        self.budget = budget
    }
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case Self.showCategories:
            if let destination = segue.destination as? DetailCategoriesViewController {
                destination.configure(with: budget) }
        case Self.editCategory:
            if let destination = segue.destination as? EditCategoryName {
                destination.configure(with: budget)
                destination.nameDelegate = self }
        default:
            print("No segue")
        }
    }
}

// MARK: Protocol Delegate
extension EditBudgetingController: NameDelegate {
    func refresh(budget: Budget) {
        self.configure(with: budget)
        self.nameLabel.text = budget.category
    }
}

