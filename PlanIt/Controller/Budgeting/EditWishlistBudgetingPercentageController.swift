//
//  EditWishlistBudgetingPercentageController.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 15/06/21.
//

import UIKit

protocol DataDelegate {
    func refresh(wishlist: [Wishlist])
}

class EditWishlistBudgetingPercentageController: UIViewController {
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var percentage1: UITextField!
    @IBOutlet weak var percentage2: UITextField!
    @IBOutlet weak var percentage3: UITextField!
    
    private var wishlist = [Wishlist]()
    var dataDelegate: DataDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        switch wishlist.count {
        case 1:
            percentage1.text = String(Int(wishlist[0].percentage))
            percentage1.isHidden = false
            percentage2.text = ""
            percentage2.isHidden = true
            percentage3.text = ""
            percentage3.isHidden = true
            label1.isHidden = false
            label2.isHidden = true
            label3.isHidden = true
        case 2:
            percentage1.text = String(Int(wishlist[0].percentage))
            percentage1.isHidden = false
            percentage2.text = String(Int(wishlist[1].percentage))
            percentage2.isHidden = false
            percentage3.text = ""
            percentage3.isHidden = true
            label1.isHidden = false
            label2.isHidden = false
            label3.isHidden = true
        case 3:
            percentage1.text = String(Int(wishlist[0].percentage))
            percentage1.isHidden = false
            percentage2.text = String(Int(wishlist[1].percentage))
            percentage2.isHidden = false
            percentage3.text = String(Int(wishlist[2].percentage))
            percentage3.isHidden = false
            label1.isHidden = false
            label2.isHidden = false
            label3.isHidden = false
        default:
            percentage1.text = ""
            percentage1.isHidden = true
            percentage2.text = ""
            percentage2.isHidden = true
            percentage3.text = ""
            percentage3.isHidden = true
            label1.isHidden = true
            label2.isHidden = true
            label3.isHidden = true
        }
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        var totalPercentage: Float = 0
        switch wishlist.count {
        case 1:
            wishlist[0].percentage = Float(percentage1.text ?? "0") ?? 0
            totalPercentage = wishlist[0].percentage
        case 2:
            wishlist[0].percentage = Float(percentage1.text ?? "0") ?? 0
            wishlist[1].percentage = Float(percentage2.text ?? "0") ?? 0
            totalPercentage = wishlist[0].percentage + wishlist[1].percentage
        case 3:
            wishlist[0].percentage = Float(percentage1.text ?? "0") ?? 0
            wishlist[1].percentage = Float(percentage2.text ?? "0") ?? 0
            wishlist[2].percentage = Float(percentage3.text ?? "0") ?? 0
            totalPercentage = wishlist[0].percentage + wishlist[1].percentage + wishlist[2].percentage
        default:
            print("No item")
        }
        
        if totalPercentage == 100 {
            DataManager.shared.saveData()
            
            let wishlistDataDict:[String: [Wishlist]] = ["wishlist": wishlist]
            let name = Notification.Name(percentNotificationKey)
            NotificationCenter.default.post(name: name, object: nil, userInfo: wishlistDataDict)
            
            dataDelegate.refresh(wishlist: self.wishlist)
            
            dismiss(animated: true, completion: nil)
        } else {
            let ac = UIAlertController(title: "Unable to Save", message: "Total percentage \nneeds to be 100", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(ac, animated: true, completion: nil)
        }
    }
}

// MARK: Data Transmission
extension EditWishlistBudgetingPercentageController {
    func configure(with wishlist: [Wishlist]) {
        self.wishlist = wishlist
    }
}
