//
//  BudgetingViewController.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 10/06/21.
//

import Foundation
import UIKit
import CoreData

class BudgetingViewController: UITableViewController {
    private let context = DataManager.shared.context
    private var salary = [Salary]()
    private var budgetData = [Budget]()
    private var wishlistData = [Wishlist]()
    
    static var budgetFirstCategory: String?
    
    private let newWishlistKey = Notification.Name(newWishlistNotificationKey)
    private let deleteWishlistKey = Notification.Name(deleteWishlistNotificationKey)
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchItems()
        
        guard let budget = salary.first?.budget,
              let wishlist = salary.first?.wishlist else { fatalError("No salary data") }
        
        if let budgetArray = budget.allObjects as? [Budget] {
            let budgetSorted = budgetArray.sorted {
                return $0.id < $1.id
            }
            
            self.budgetData = budgetSorted
        }
        
        if let wishlistArray = wishlist.allObjects as? [Wishlist] {
            let wishlistSorted = wishlistArray.sorted {
                return $0.percentage > $1.percentage
            }
            
            self.wishlistData = wishlistSorted
        }
        
        if let category = budgetData.first?.category {
            BudgetingViewController.budgetFirstCategory = category
        }
        
        createObserver()
    }
}

// MARK: -TableView
extension BudgetingViewController {
    static let salaryCellIdentifier = "salary"
    static let budgetingCellIdentifier = "budgeting"
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let budget = salary.first?.budget else { fatalError("No budget data") }
        if wishlistData.isEmpty {
            return budget.count + 2
        } else {
            return budget.count + 3
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let data = salary.first,
              let budget = data.budget else { fatalError("No budget data") }
        
        switch (indexPath.row, wishlistData.count) {
        case (0, _):
            guard let salaryCell = tableView.dequeueReusableCell(withIdentifier: Self.salaryCellIdentifier) as? SalaryCell else { fatalError("Unable to dequeue salary Cell") }
            
            if data.amount > 0 {
                if let formattedSalary = DataFormatter.shared.formatToIDR(data.amount) {
                    salaryCell.salaryLabel.text = "Rp. " + formattedSalary
                }
            } else {
                salaryCell.salaryLabel.text = "We need salary"
            }
            
            salaryCell.salaryBG.backgroundColor = Colors.yellow
            salaryCell.salaryBG.layer.cornerRadius = 25
            return salaryCell
        case (1, _):
            guard let headerCell = tableView.dequeueReusableCell(withIdentifier: "title") else { fatalError("Unable to dequeue header Cell") }
            return headerCell
        case (budget.count + 2, let x) where x != 0:
            guard let wishlistCell = tableView.dequeueReusableCell(withIdentifier: "wishlist") as? WishlistCell else { fatalError("Unable to deque wishlist cell") }
            
            var progress: Float = 0
            var total: Float = 0
            for wish in self.wishlistData {
                let progressData = wish.wishlistHistory as! Set<WishlistHistory>
                for prog in progressData {
                    progress += prog.progressAmount
                }
                
                total += wish.total
            }
            
            if let total = DataFormatter.shared.formatToIDR(total),
               let prog = DataFormatter.shared.formatToIDR(progress) {
                wishlistCell.wishlistProgress.text = "Rp. " + prog + " / Rp. " + total
            }
            wishlistCell.progressBar.progress = progress / total
            
            wishlistCell.wishlistView.backgroundColor = .systemBackground
            wishlistCell.wishlistView.layer.cornerRadius = 25
            wishlistCell.wishlistView.layer.shadowRadius = CGFloat(7.0)
            wishlistCell.wishlistView.layer.shadowOffset = CGSize(width: 0, height: 4)
            wishlistCell.wishlistView.layer.shadowOpacity = 0.1
            
            if let image = wishlistData.first?.imageData {
                wishlistCell.wishlistImage.image = UIImage(data: image)
                wishlistCell.wishlistImage.roundCorners(corners: [.topLeft, .topRight], radius: 25)
            }
            return wishlistCell
        default:
            guard let budgetingCell = tableView.dequeueReusableCell(withIdentifier: Self.budgetingCellIdentifier) as? BudgetingCell else { fatalError("Unable to dequeue Budgeting Cell") }
            
            let dataBudgeting = budgetData[indexPath.row - 2]
            
            budgetingCell.categoriesView.layer.cornerRadius = 25
            budgetingCell.categoriesView.layer.shadowRadius = CGFloat(7.0)
            budgetingCell.categoriesView.layer.shadowOffset = CGSize(width: 0, height: 4)
            budgetingCell.categoriesView.layer.shadowOpacity = 0.1
            
            if let image = dataBudgeting.imageData {
                budgetingCell.categoriesImage.image = UIImage(data: image)
                budgetingCell.categoriesImage.roundCorners(corners: [.topLeft, .topRight], radius: 25)
                budgetingCell.categoriesImage.contentMode = UIView.ContentMode.scaleAspectFill
                budgetingCell.categoriesImage.clipsToBounds = true
            }
            
            if let expenseHistory = dataBudgeting.expenseHistory,
               let histData = expenseHistory as? Set<ExpenseHistory> {
                var spending: Float = 0
                for expense in histData {
                    spending += expense.detailAmount
                }
                
                let totalTemp = data.amount * dataBudgeting.percentage / 100
                
                if let total = DataFormatter.shared.formatToIDR(totalTemp),
                   let spending = DataFormatter.shared.formatToIDR(totalTemp - spending) {
                    budgetingCell.categoriesAmount.text = "Rp. " + spending + " / Rp. " + total
                }
                
                let budgetProgress: Float = (totalTemp - spending) / totalTemp
                budgetingCell.progressBudget.progress = budgetProgress
            }
            
            budgetingCell.categoriesLabel.text = dataBudgeting.category
            return budgetingCell
        }
    }
    
}

// MARK: Data Transmission
extension BudgetingViewController {
    static let showAddSalary = "addSalary"
    static let showDetail = "showDetail"
    static let addTransaction = "addTransaction"
    static let showBudgetingWishlist = "showBudgetingWishlist"
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case Self.showAddSalary:
            if let destination = segue.destination as? AddSalaryController{
                destination.configure(with: salary)
                destination.salaryDelegate = self
            }
        case Self.showBudgetingWishlist:
            if let destination = segue.destination as? WishlistBudgetingController{
                destination.configure(with: salary)
                destination.firstWishlistDelegate = self
            }
        case Self.addTransaction:
            if let destination = segue.destination as? AddTransactionController,
               let data = salary.first {
                destination.configure(with: data)
                destination.addTransactionDelegate = self
            }
        case Self.showDetail:
            if let destination = segue.destination as? BudgetingDetailViewController,
               let cell = sender as? UITableViewCell,
               let indexPath = tableView.indexPath(for: cell),
               let amount = salary.first?.amount {
                destination.configure(with: budgetData[indexPath.row - 2], and: amount)
                destination.categoryNameDelegate = self
            }
        default:
            print("No segue")
        }
    }
}

// MARK: Fetch Core Data
extension BudgetingViewController: CoreDataFetchDelegate {
    func fetchItems() {
        do {
            let request: NSFetchRequest<Salary> = Salary.fetchRequest()
            request.returnsObjectsAsFaults = false
            self.salary = try context.fetch(request)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } catch {
            print(error)
        }
    }
}

// MARK: Observer Notification
extension BudgetingViewController {
    func createObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateDataObs(_:)), name: newWishlistKey, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateDeletedDataObs(_:)), name: deleteWishlistKey, object: nil)
    }
    
    @objc func updateDeletedDataObs(_ notification: NSNotification) {
        if let dict = notification.userInfo as NSDictionary?,
           let data = dict["wishlist"] as? [Wishlist] {
            let wishlistSorted = data.sorted {
                return $0.percentage > $1.percentage
            }
            
            self.wishlistData = wishlistSorted
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
        
    }

    @objc func updateDataObs(_ notification: NSNotification) {
        guard let dict = notification.userInfo as NSDictionary?,
              let data = dict["wishlist"] as? [Wishlist] else { fatalError("Notification info not found") }
        
        let wishlistSorted = data.sorted {
            return $0.percentage > $1.percentage
        }
        
        self.wishlistData = wishlistSorted
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

// MARK: Protocol Delegate
extension BudgetingViewController: AddTransactionDelegate {
    func refreshTransaction(budget: [Budget]) {
        let budgetSorted = budget.sorted {
            return $0.id < $1.id
        }
        self.budgetData = budgetSorted
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

extension BudgetingViewController: FirstWishlistDelegate {
    func refreshWishlist(wishlist: [Wishlist]) {
        self.wishlistData = wishlist
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

extension BudgetingViewController: CategoryNameDelegate {
    func refresh(budget: Budget) {
        for bud in budgetData {
            if budget.id == bud.id {
                budgetData[Int(budget.id)] = budget
            }
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

extension BudgetingViewController: SalaryDelegate {
    func refresh() {
        fetchItems()
    }
}
