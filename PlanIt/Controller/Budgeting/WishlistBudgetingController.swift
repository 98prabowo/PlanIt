//
//  WishlistBudgetingController.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 14/06/21.
//

import Foundation
import UIKit

protocol FirstWishlistDelegate {
    func refreshWishlist(wishlist: [Wishlist])
}

class WishlistBudgetingController: UITableViewController {
    private var salary: Salary?
    private var wishlistData = [Wishlist]()
    var firstWishlistDelegate: FirstWishlistDelegate!
    
    private let newWishlistKey = Notification.Name(newWishlistNotificationKey)
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let salary = self.salary,
              let wishlist = salary.wishlist else { fatalError("No budget data") }
        
        if let wishlistArray = wishlist.allObjects as? [Wishlist] {
            let wishlistSorted = wishlistArray.sorted {
                return $0.percentage > $1.percentage
            }
            
            self.wishlistData = wishlistSorted
        }

        createObserver()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        firstWishlistDelegate.refreshWishlist(wishlist: wishlistData)
    }
    
    // MARK: Observer Notification
    func createObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateDataObs(_:)), name: newWishlistKey, object: nil)
    }

    @objc func updateDataObs(_ notification: NSNotification) {
        guard let dict = notification.userInfo as NSDictionary?,
              let data = dict["wishlist"] as? [Wishlist] else { fatalError("Notification info not found") }
        
        let wishlistSorted = data.sorted {
            return $0.percentage > $1.percentage
        }
        
        self.wishlistData = wishlistSorted
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

//MARK: TableView
extension WishlistBudgetingController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let data = salary,
              let wishlist = data.wishlist else { fatalError("No wishlist data") }
        return wishlist.count + 2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let data = salary else { fatalError("No salary data") }
        let amount = data.amount * (data.wishlistPercentage / 100)
        
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "wishlistHeader") as? BudgetingWishlistHeaderCell else { fatalError("Unable to deque header cell") }
            if let amountIDR = DataFormatter.shared.formatToIDR(Int(amount)) {
                cell.progressPerMonth.text = "Rp. " + amountIDR
            }
            
            if let image = wishlistData.first?.imageData {
                cell.wishlistImage.image = UIImage(data: image)
                cell.wishlistImage.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 20)
            }
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "subHeader") else { fatalError("Unable to deque sub header cell") }
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "wishlistPercentage") as? BudgetingWishlistHistoryCell else { fatalError("Unable to deque wishlist percentage cell") }
            
            let detailPerMonth = Int(amount * (wishlistData[indexPath.row - 2].percentage / 100))
            if let perMonthIDR = DataFormatter.shared.formatToIDR(detailPerMonth) {
                cell.detailPerMonth.text = "Rp." + perMonthIDR + "/month"
            }
            
            cell.percentage.text = "\(Int(wishlistData[indexPath.row - 2].percentage))%"
            cell.wishlist.text = wishlistData[indexPath.row - 2].name
            
            cell.wishlistBG.backgroundColor = .systemBackground
            cell.wishlistBG.layer.cornerRadius = 25
            cell.wishlistBG.layer.shadowRadius = CGFloat(7.0)
            cell.wishlistBG.layer.shadowOffset = CGSize(width: 0, height: 4)
            cell.wishlistBG.layer.shadowOpacity = 0.1
            cell.percentageBG.backgroundColor = Colors.yellow
            cell.percentageBG.layer.cornerRadius = 25
            cell.percentageBG.layer.shadowOffset = CGSize(width: 0, height: 4)
            cell.percentageBG.layer.shadowOpacity = 0.1
            return cell
        }
        
    }
}

// MARK: Data Transmission
extension WishlistBudgetingController {
    static let showEditPercentage = "editPercentage"
    
    func configure(with salary: [Salary]?) {
        if let _salary = salary?.first {
            self.salary = _salary
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Self.showEditPercentage,
           let destination = segue.destination as? EditWishlistBudgetingPercentageController {
            destination.configure(with: wishlistData)
            destination.dataDelegate = self
        }
    }
}

// MARK: Protocol Delegate
extension WishlistBudgetingController: DataDelegate {
    func refresh(wishlist: [Wishlist]) {
        self.wishlistData = wishlist.sorted{ return $0.percentage > $1.percentage }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
