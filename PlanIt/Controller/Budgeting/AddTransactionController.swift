//
//  AddTransactionController.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 14/06/21.
//

import Foundation
import UIKit

protocol AddTransactionDelegate {
    func refreshTransaction(budget: [Budget])
}

class AddTransactionController: UIViewController {
    @IBOutlet weak var date: UITextField!
    @IBOutlet weak var transactionAmount: UITextField!
    @IBOutlet weak var chooseCategory: UITextField!
    
    private let context = DataManager.shared.context
    private var salary: Salary?
    private var budgetData = [Budget]()
    private var categories = [String]()
    var addTransactionDelegate: AddTransactionDelegate!
    
    private lazy var datePicker = UIDatePicker()
    private lazy var categoryPicker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createDatePicker()
        
        date.placeholder = "Choose date"
        
        guard let salary = self.salary,
              let budget = salary.budget else { fatalError("No budget data") }
        
        if let budgetSet = budget as? Set<Budget> {
            for (_, bud) in budgetSet.enumerated() {
                self.budgetData.append(bud)
                
                if let detailCategories = bud.detailCategories {
                    for category in detailCategories {
                        self.categories.append(category)
                    }
                }
            }
        }

        categoryPicker.delegate = self
        categoryPicker.dataSource = self
        chooseCategory.inputView = categoryPicker
    }
    
    func createDatePicker() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([doneBtn], animated: true)
        
        date.inputAccessoryView = toolbar
        date.inputView = datePicker
        
        datePicker.datePickerMode = .date
    }
    
    @objc func donePressed() {
        date.text = DataFormatter.shared.formatDate().string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        if let date = date.text,
           let amountString = transactionAmount.text,
           let amount = Float(amountString),
           let category = chooseCategory.text {
            for budget in budgetData {
                if let detail = budget.detailCategories {
                    for detailCategory in detail {
                        if detailCategory == category {
                            let expense = ExpenseHistory(context: context)
                            expense.category = category
                            expense.dateString = date
                            expense.detailAmount = amount
                            expense.dateData = DataFormatter.shared.formatDate().date(from: date)
                            
                            budget.addToExpenseHistory(expense)
                        }
                    }
                } else {
                    let ac = UIAlertController(title: "Missing Data", message: "You need to input all data", preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    present(ac, animated: true, completion: nil)
                }
            }
            
            DataManager.shared.saveData()
            addTransactionDelegate.refreshTransaction(budget: budgetData)
        }
        
        dismiss(animated: true, completion: nil)
    }
}

// MARK: PickerView
extension AddTransactionController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categories[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        chooseCategory.text = categories[row]
        chooseCategory.resignFirstResponder()
    }
}

// MARK: Data Transmission
extension AddTransactionController {
    func configure(with salary: Salary) {
        self.salary = salary
    }
}
