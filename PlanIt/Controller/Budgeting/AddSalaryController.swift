//
//  addSalary.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 10/06/21.
//

import Foundation
import UIKit

protocol SalaryDelegate {
    func refresh()
}

class AddSalaryController: UIViewController {
    @IBOutlet weak var salaryAmount: UITextField!
    @IBOutlet weak var budgetPercentageA: UITextField!
    @IBOutlet weak var budgetPercentageB: UITextField!
    @IBOutlet weak var budgetPercentageC: UITextField!
    @IBOutlet weak var wishlistPercentage: UITextField!
    
    @IBOutlet weak var budgetA: UILabel!
    @IBOutlet weak var budgetB: UILabel!
    @IBOutlet weak var budgetC: UILabel!
    
    private var salary: Salary?
    private var budgetData = [Budget]()
    var salaryDelegate: SalaryDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let data = self.salary,
              let budget = data.budget else { fatalError("Budget data not found") }
        
        if let budgetArray = budget.allObjects as? [Budget] {
            let budgetSorted = budgetArray.sorted {
                return $0.id < $1.id
            }
            
            self.budgetData = budgetSorted
        }
        
        salaryAmount.text = "\(Int(data.amount))"
        budgetPercentageA.text = "\(Int(budgetData[0].percentage))"
        budgetPercentageB.text = "\(Int(budgetData[1].percentage))"
        budgetPercentageC.text = "\(Int(budgetData[2].percentage))"
        wishlistPercentage.text = "\(Int(data.wishlistPercentage))"
        
        if let budgetA = budgetData[0].category,
           let budgetB = budgetData[1].category,
           let budgetC = budgetData[2].category {
            self.budgetA.text = budgetA
            self.budgetB.text = budgetB
            self.budgetC.text = budgetC
            
            budgetPercentageA.placeholder = "Enter your \(budgetA) percentage"
            budgetPercentageB.placeholder = "Enter your \(budgetB) percentage"
            budgetPercentageC.placeholder = "Enter your \(budgetC) percentage"
        }
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        guard let data = salary else { return }
        if let amountString = salaryAmount.text,
           let percentAString = budgetPercentageA.text,
           let percentBString = budgetPercentageB.text,
           let percentCString = budgetPercentageC.text,
           let percentWishlistString = wishlistPercentage.text,
           let amount = Float(amountString),
           let percentA = Float(percentAString),
           let percentB = Float(percentBString),
           let percentC = Float(percentCString),
           let percentWishlist = Float(percentWishlistString) {
            data.amount = amount
            data.wishlistPercentage = percentWishlist
            budgetData[0].percentage = percentA
            budgetData[1].percentage = percentB
            budgetData[2].percentage = percentC
            
            if (percentA + percentB + percentC + percentWishlist) == 100 {
                DataManager.shared.saveData()
                
                salaryDelegate.refresh()
                dismiss(animated: true, completion: nil)
            } else {
                let ac = UIAlertController(title: "Unable to Save", message: "Total percentage \nneeds to be 100", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(ac, animated: true, completion: nil)
            }
        }
    }
}

// MARK: Data Transmission
extension AddSalaryController {
    func configure(with salary: [Salary]?) {
        if let _salary = salary?.first {
            self.salary = _salary
        }
    }
}
