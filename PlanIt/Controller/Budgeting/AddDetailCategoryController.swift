//
//  AddDetailCategoryController.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 14/06/21.
//

import Foundation
import UIKit

protocol AddCategoryDelegate {
    func refresh(budget: Budget)
}

class AddDetailCategoryController: UIViewController {
    @IBOutlet weak var newCategory: UITextField!
    
    private var budget: Budget?
    var addCategoryDelegate: AddCategoryDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        if let new = newCategory.text,
           let data = self.budget,
           var categories = data.detailCategories {
            categories.append(new)
            data.detailCategories = categories
            
            DataManager.shared.saveData()
            
            addCategoryDelegate.refresh(budget: data)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: Data Transmision
extension AddDetailCategoryController {
    func configure(with budget: Budget?) {
        self.budget = budget
    }
}
