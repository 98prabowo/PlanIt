//
//  EditCategoryName.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 14/06/21.
//

import Foundation
import UIKit

protocol NameDelegate {
    func refresh(budget: Budget)
}

class EditCategoryName: UIViewController {
    @IBOutlet weak var categoryName: UITextField!
    
    private var budget: Budget?
    var nameDelegate: NameDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let data = self.budget else { return }
        categoryName.text = data.category
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        if let category = categoryName.text,
           let data = budget {
            data.category = category
            DataManager.shared.saveData()
            
            nameDelegate.refresh(budget: data)
        }
        dismiss(animated: true, completion: nil)
    }
}

// MARK: Data Transmission
extension EditCategoryName {
    func configure(with budget: Budget?) {
        self.budget = budget
    }
}
