//
//  BudgetingDetailViewController.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 10/06/21.
//

import Foundation
import UIKit

protocol CategoryNameDelegate {
    func refresh(budget: Budget)
}

class BudgetingDetailViewController: UITableViewController {
    private var budget: Budget?
    private var salaryAmount: Float?
    private var collectionView: UICollectionView?
    var categoryNameDelegate: CategoryNameDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let data = self.budget else { return }
        self.navigationItem.title = data.category
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let data = budget {
            categoryNameDelegate.refresh(budget: data)
        }
    }
}

// - MARK: TableView
extension BudgetingDetailViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let data = budget,
              let _ = data.detailCategories else { return 1 }
        guard let history = data.expenseHistory else { return 2 }
        
        let maxExpenseShowed = 15
        if history.count == 0 {
            return 2
        } else if history.count > 15 {
            return maxExpenseShowed + 3
        } else {
            return history.count + 3
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let data = budget else { fatalError("No budget data") }
        switch indexPath.row {
        case 0:
            guard let headerCell = tableView.dequeueReusableCell(withIdentifier: "header") as? DetailHeaderCell else { fatalError("Unable to get header data") }
            
            if let expenseHistory = data.expenseHistory,
               let amount = salaryAmount {
                let histData = expenseHistory as! Set<ExpenseHistory>
                var spending: Float = 0
                for expense in histData {
                    spending += expense.detailAmount
                }
                
                let totalTemp = amount * data.percentage / 100
                
                if let expense = DataFormatter.shared.formatToIDR(totalTemp - spending),
                   let total = DataFormatter.shared.formatToIDR(totalTemp) {
                    headerCell.detailSpended.text = "Rp. " + expense
                    headerCell.detailTotal.text = "Rp. " + total
                }
            }
            
            if let image = data.imageData {
                headerCell.detailImage.image = UIImage(data: image)
                headerCell.detailImage.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 20)
            }
            
            return headerCell
        case 1:
            guard let categoriesCell = tableView.dequeueReusableCell(withIdentifier: "categories") as? MainCategoriesCell else { fatalError("Unable to deque categories") }
            let collectionView = categoriesCell.categoriesCell
            self.collectionView = collectionView
            let layout = UICollectionViewCenterLayout()
            collectionView!.collectionViewLayout = layout
            return categoriesCell
        case 2:
            guard let subHeaderCell = tableView.dequeueReusableCell(withIdentifier: "subHeader") else { fatalError("Unable to deque subheader") }
            return subHeaderCell
        default:
            guard let historyCell = tableView.dequeueReusableCell(withIdentifier: "history") as? HistoryCell else { fatalError("Unable to deque history") }
            
            if let history = data.expenseHistory?.allObjects as? [ExpenseHistory] {
                let histData = history.sorted{
                    if let initial = $0.dateData,
                       let next = $1.dateData {
                        return initial > next
                    } else {
                        return false
                    }
                }
                
                if let amount = DataFormatter.shared.formatToIDR(histData[indexPath.row - 3].detailAmount),
                   let category = histData[indexPath.row - 3].category,
                   let date = histData[indexPath.row - 3].dateData {
                    historyCell.expensesAmount.text = "-\(amount)"
                    historyCell.categoryExpenses.text = category
                    historyCell.expensesDate.text = DataFormatter.shared.formatDate().string(from: date)
                }
            }
            
            historyCell.dateBG.backgroundColor = Colors.yellow
            historyCell.dateBG.roundCorners(corners: [.topLeft, .bottomRight], radius: 25)
            
            historyCell.historyBG.backgroundColor = .systemBackground
            historyCell.historyBG.layer.cornerRadius = 25
            historyCell.historyBG.layer.shadowRadius = CGFloat(7.0)
            historyCell.historyBG.layer.shadowOffset = CGSize(width: 0, height: 4)
            historyCell.historyBG.layer.shadowOpacity = 0.1
             
            return historyCell
        }
    }
}

// - MARK: CollectionView
extension BudgetingDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    static let categoriesID = "categoriesID"

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let data = budget,
              let categories = data.detailCategories else { return 0 }
        
        if let category = BudgetingViewController.budgetFirstCategory,
           data.category == category {
            return categories.count - 1
        } else {
            return categories.count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Self.categoriesID, for: indexPath) as? InsideCategoriesCell else { fatalError("Unable to dequeue categories") }
        
        guard let data = budget,
              let categories = data.detailCategories else { fatalError("No Categories") }
        
        if let category = BudgetingViewController.budgetFirstCategory,
           data.category == category {
            cell.categoriesLabel.text = "  " + categories[indexPath.row + 1] + "  "
            cell.categoriesLabel.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        } else {
            cell.categoriesLabel.text = "  " + categories[indexPath.row] + "  "
            cell.categoriesLabel.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        }
        
        cell.backgroundColor = Colors.yellow
        cell.layer.cornerRadius = 10
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let data = budget,
              let categories = data.detailCategories else { fatalError("No Categories") }
        
        var width: CGFloat = 0
        if let category = BudgetingViewController.budgetFirstCategory,
           data.category == category {
            width = CGFloat((categories[indexPath.row + 1].count + 8) * 6)
        } else {
            width = CGFloat((categories[indexPath.row].count + 8) * 6)
        }
        
        return CGSize(width: width, height: 30)
    }
}

// MARK: Data Transmission
extension BudgetingDetailViewController {
    static let showEdit = "showEdit"
    
    // Get Data
    func configure(with budget: Budget?, and amount: Float) {
        self.budget = budget
        self.salaryAmount = amount
    }
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Self.showEdit,
           let destination = segue.destination as? EditBudgetingController{
            if let data = budget {
                destination.configure(with: data)
                destination.editBudgetingDelegate = self
            }
        }
    }
}

// MARK: Protocol Delegate
extension BudgetingDetailViewController: EditBudgetingDelegate {
    func refresh(budget: Budget) {
        self.budget = budget
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.navigationItem.title = budget.category
            if let cv = self.collectionView {
                cv.reloadData()
            }
        }
    }
}
