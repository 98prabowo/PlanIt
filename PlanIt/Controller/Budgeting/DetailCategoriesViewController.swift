//
//  DetailCategories.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 14/06/21.
//

import Foundation
import UIKit

class DetailCategoriesViewController: UITableViewController {
    private var budget: Budget?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @objc func deleteTapped(sender: UIButton) {
        guard let data = self.budget,
              var categories = data.detailCategories else { return }
        let buttonTag = sender.tag
        categories.remove(at: buttonTag)
        data.detailCategories = categories
        
        DataManager.shared.saveData()
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

// MARK: TableView
extension DetailCategoriesViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let data = budget,
              let categories = data.detailCategories else { fatalError() }
        if let category = BudgetingViewController.budgetFirstCategory,
           data.category == category {
            return categories.count - 1
        } else {
            return categories.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? DetailCategoriesCell else { fatalError("Unable deque detail categories cell") }
        guard let data = budget,
              let categories = data.detailCategories else { fatalError() }
        
        cell.categoriesBG.backgroundColor = .systemBackground
        cell.categoriesBG.layer.cornerRadius = 20
        cell.categoriesBG.layer.borderWidth = 2
        cell.categoriesBG.layer.borderColor = Colors.yellow.cgColor
        
        if let category = BudgetingViewController.budgetFirstCategory,
           data.category == category {
            cell.categoriesLabel.text = categories[indexPath.row + 1]
            cell.deleteButton.tag = indexPath.row + 1
            cell.deleteButton.addTarget(self, action: #selector(deleteTapped(sender:)), for: .touchUpInside)
        } else {
            cell.categoriesLabel.text = categories[indexPath.row]
            cell.deleteButton.tag = indexPath.row
            cell.deleteButton.addTarget(self, action: #selector(deleteTapped(sender:)), for: .touchUpInside)
        }
        
        return cell
    }
}

// MARK: Data Transmission
extension DetailCategoriesViewController {
    static let addDetailCategories = "addDetailCategories"
    
    func configure(with budget: Budget?) {
        self.budget = budget
    }
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Self.addDetailCategories,
           let destination = segue.destination as? AddDetailCategoryController {
            destination.configure(with: budget)
            destination.addCategoryDelegate = self
        }
    }
}

// MARK: Protocol Delegate
extension DetailCategoriesViewController: AddCategoryDelegate {
    func refresh(budget: Budget) {
        self.configure(with: budget)
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
