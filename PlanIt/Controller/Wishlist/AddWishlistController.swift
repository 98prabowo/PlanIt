//
//  AddWishlistController.swift
//  PlanIt
//
//  Created by Dimas Prabowo on 17/06/21.
//

import UIKit

protocol AddNewWishlist {
    func refreshWishlist(wishlist: [Wishlist])
}

class AddWishlistController: UIViewController {
    @IBOutlet weak var wishlistImage: UIImageView!
    @IBOutlet weak var wishlistName: UITextField!
    @IBOutlet weak var wishlistPrice: UITextField!
    
    private let context = DataManager.shared.context
    private var salary: Salary?
    private var budget: [Budget]?
    private var wishlist: [Wishlist]?
    var addNewWishlist: AddNewWishlist!
    
    private lazy var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        wishlistImage.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 25)
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        if let salary = salary,
           var data = wishlist,
           let budget = budget?.first,
           let name = wishlistName.text,
           let priceString = wishlistPrice.text,
           let price = Float(priceString),
           let imageData = wishlistImage.image?.pngData() {
            
            if data.count < 3 {
                var totalPercentage: Float = 0
                for percent in data {
                    totalPercentage += percent.percentage
                }
                
                let progress = WishlistHistory(context: context)
                progress.progressAmount = 100_000
                progress.dateData = Date(timeIntervalSinceNow: 0)
                
                let newWishlist = Wishlist(context: context)
                newWishlist.id = UUID()
                newWishlist.name = name
                newWishlist.total = price
                newWishlist.progress = progress.progressAmount
                newWishlist.imageData = imageData
                
                newWishlist.addToWishlistHistory(progress)
                if !data.isEmpty {
                    newWishlist.percentage = 10
                    data[0].percentage -= newWishlist.percentage
                } else {
                    newWishlist.percentage = 100
                }
                
                let expense = ExpenseHistory(context: context)
                expense.category = "Wishlist"
                expense.dateData = Date(timeIntervalSinceNow: 0)
                expense.detailAmount = progress.progressAmount
                budget.addToExpenseHistory(expense)
                
                salary.addToWishlist(newWishlist)
                
                data.append(newWishlist)
                let wishlistDataDict:[String: [Wishlist]] = ["New Wishlist": data]
                let name = Notification.Name(newWishlistNotificationKey)
                NotificationCenter.default.post(name: name, object: nil, userInfo: wishlistDataDict)
                
                DataManager.shared.saveData()
            } else {
                let ac = UIAlertController(title: "Missing Data", message: "You need to input all data", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                present(ac, animated: true, completion: nil)
            }
            
            addNewWishlist.refreshWishlist(wishlist: data)
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func editImageTapped(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
}

// MARK: Data Transmission
extension AddWishlistController {
    func configure(with wishlist: [Wishlist], and salary: Salary) {
        self.wishlist = wishlist
        self.salary = salary
        
        if let budget = self.salary?.budget,
           let budgetArray = budget.allObjects as? [Budget] {
            self.budget = budgetArray.sorted {
                return $0.percentage > $1.percentage
            }
        }
    }
}

// MARK: Image Picker
extension AddWishlistController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imageData = info[.editedImage] as? UIImage {
            wishlistImage.image = imageData
        }
        dismiss(animated: true, completion: nil)
    }
}
