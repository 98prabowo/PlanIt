//
//  EditWishlistPriceViewController.swift
//  PlanIt
//
//  Created by Yanandra Dhafa on 16/06/21.
//  Maintained by Dimas Prabowo since 17/06/21.
//

import UIKit

protocol WishlistPriceDelegate {
    func refreshPrice(wishlist: Wishlist)
}

class EditWishlistPriceViewController: UIViewController {
    @IBOutlet weak var priceEdit: UITextField!
    
    private var wishlist: Wishlist?
    var wishlistPriceDelegate: WishlistPriceDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let data = wishlist else { return }
        priceEdit.text = "\(Int(data.total))"
        priceEdit.placeholder = "Enter your wishlist price"
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        if let priceString = priceEdit.text,
           let price = Float(priceString),
           let data = wishlist {
            data.total = price
            DataManager.shared.saveData()
            wishlistPriceDelegate.refreshPrice(wishlist: data)
        }
        
        dismiss(animated: true, completion: nil)
    }
}

// MARK: Data Transmission
extension EditWishlistPriceViewController {
    func configure(with wishlist: Wishlist) {
        self.wishlist = wishlist
    }
}
