//
//  EditWishlistViewController.swift
//  PlanIt
//
//  Created by Yanandra Dhafa on 14/06/21.
//  Maintained by Dimas Prabowo since 17/06/21.
//

import UIKit

protocol EditDetailWishlistDelegate {
    func refresh(wishlist: Wishlist)
}

class EditWishlistController: UIViewController {
    @IBOutlet weak var editWishlistImage: UIImageView!
    @IBOutlet weak var wishlistName: UILabel!
    @IBOutlet weak var buttonName: UIButton!
    @IBOutlet weak var wishlistPrice: UILabel!
    @IBOutlet weak var buttonPrice: UIButton!
    @IBOutlet weak var deleteWishlist: UIButton!
    
    private var wishlist: Wishlist?
    var editDetailWishlistDelegate: EditDetailWishlistDelegate!
    private lazy var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let data = wishlist,
           let image = data.imageData,
           let price = DataFormatter.shared.formatToIDR(data.total){
            wishlistName.text = data.name
            wishlistPrice.text = "Rp. \(price)"
            editWishlistImage.image = UIImage(data: image)
            editWishlistImage.roundCorners(corners:[.bottomLeft,.bottomRight], radius: 25)
        } else {
            wishlistName.text = "Your Wishlist"
            wishlistPrice.text = "-"
        }
        
        wishlistPrice.isUserInteractionEnabled = false
        wishlistName.isUserInteractionEnabled = false
        editWishlistImage.contentMode = .scaleAspectFill
        
        buttonName.backgroundColor = .clear
        buttonName.layer.cornerRadius = 20
        buttonName.layer.borderColor = Colors.yellow.cgColor
        buttonName.layer.borderWidth = 2
        buttonPrice.backgroundColor = .clear
        buttonPrice.layer.cornerRadius = 20
        buttonPrice.layer.borderColor = Colors.yellow.cgColor
        buttonPrice.layer.borderWidth = 2
        deleteWishlist.backgroundColor = .clear
        deleteWishlist.layer.cornerRadius = 20
        deleteWishlist.layer.borderColor = UIColor.systemRed.cgColor
        deleteWishlist.layer.borderWidth = 2
        
        imagePicker.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let data = wishlist {
            if let imageData = editWishlistImage.image?.pngData() {
                data.imageData = imageData
                
                DataManager.shared.saveData()
            }
            editDetailWishlistDelegate.refresh(wishlist: data)
        }
    }
    
    @IBAction func editImage(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
}

// MARK: Image Picker
extension EditWishlistController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imageData = info[.editedImage] as? UIImage {
            self.editWishlistImage.image = imageData
        }
        dismiss(animated: true, completion: nil)
    }
}

// MARK: Data Transmission
extension EditWishlistController {
    static let editWishlistName = "editWishlistName"
    static let editWishlistPrice = "editWishlistPrice"
    
    func configure(with wishlist: Wishlist) {
        self.wishlist = wishlist
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let data = wishlist else { return }
        
        if segue.identifier == Self.editWishlistName {
            let destination = segue.destination as! EditWishlistNameViewController
            destination.configure(with: data)
            destination.wishlistNameDelegate = self
        } else if segue.identifier == Self.editWishlistPrice {
            let destination = segue.destination as! EditWishlistPriceViewController
            destination.configure(with: data)
            destination.wishlistPriceDelegate = self
        } else if let destination = segue.destination as? WishlistViewControllers {
            destination.passedWishlist = wishlist
        }
    }
}

// MARK: Protocol Delegate
extension EditWishlistController: WishlistNameDelegate {
    func refresh(wishlist: Wishlist) {
        self.wishlist = wishlist
        self.wishlistName.text = wishlist.name
    }
}

extension EditWishlistController: WishlistPriceDelegate {
    func refreshPrice(wishlist: Wishlist) {
        self.wishlist = wishlist
        if let price = DataFormatter.shared.formatToIDR(Int(wishlist.total)) {
            self.wishlistPrice.text = "Rp. \(price)"
        }
    }
}
