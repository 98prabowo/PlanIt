//
//  EditWishlistNameViewController.swift
//  PlanIt
//
//  Created by Yanandra Dhafa on 16/06/21.
//  Maintained by Dimas Prabowo since 17/06/21.
//

import UIKit

protocol WishlistNameDelegate {
    func refresh(wishlist: Wishlist)
}

class EditWishlistNameViewController: UIViewController {
    @IBOutlet weak var wishListName: UITextField!
    
    private var wishlist: Wishlist?
    var wishlistNameDelegate: WishlistNameDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let data = wishlist else { return }
        wishListName.text = data.name
        wishListName.placeholder = "Insert your wishlist name"
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func saveTapped(_ sender: Any) {
        if let name = wishListName.text,
           let data = wishlist {
            data.name = name
            DataManager.shared.saveData()
            wishlistNameDelegate.refresh(wishlist: data)
        }
        dismiss(animated: true, completion: nil)
    }
}

// MARK: Data Transmission
extension EditWishlistNameViewController {
    func configure(with wishlist: Wishlist) {
        self.wishlist = wishlist
    }
}
