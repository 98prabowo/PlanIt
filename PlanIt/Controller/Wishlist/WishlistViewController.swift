//
//  UiTableViewController.swift
//  PlanIt
//
//  Created by Yanandra Dhafa on 09/06/21.
//  Maintained by Dimas Prabowo since 17/06/21.
//

import Foundation
import UIKit
import CoreData

class WishlistViewControllers: UITableViewController, UIGestureRecognizerDelegate {
    @IBOutlet var tableview: UITableView!
    
    private let context = DataManager.shared.context
    private let percentUpdate = Notification.Name(percentNotificationKey)
    var passedWishlist: Wishlist?
    private var salary = [Salary]()
    private var wishlistData = [Wishlist]()
    private var longPressed = false
    private var selectedRow = 0
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchItems()
        
        if salary.isEmpty {
            DataApps.shared.populateData()
            
            fetchItems()
        }
        
        let gesture = UISwipeGestureRecognizer(target: self, action: #selector(gesture(gesture:)))
        view.addGestureRecognizer(gesture)
        gesture.direction = .left
        gesture.numberOfTouchesRequired = 1
        gesture.delegate = self
        self.tableView.addGestureRecognizer(gesture)
        self.tableview.isUserInteractionEnabled = true
        
        if let wishlist = salary.first?.wishlist,
           let wishlistArray = wishlist.allObjects as? [Wishlist] {
            let wishlistSorted = wishlistArray.sorted {
                return $0.percentage > $1.percentage
            }
            self.wishlistData = wishlistSorted
        }
        
        if wishlistData.count >= 3 {
            navigationItem.rightBarButtonItem?.isEnabled = false
        }
        
        createObserver()
        
        payday()
    }
    
    // MARK: Wishlist Achieved
    func wishlistAchieved() {
        guard let salary = salary.first else { fatalError("Salary data not found") }
        for wish in wishlistData {
            if wish.progress >= wish.total {
                let percent = wish.percentage
                salary.removeFromWishlist(wish)
                wishlistData[0].percentage += percent
                
                DataManager.shared.saveData()
            }
        }
    }
    
    // MARK: Payday
    func payday() {
        var progressData = [WishlistHistory]()
        
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            if let progress = self?.wishlistData.first?.wishlistHistory,
               let progressArray = progress.allObjects as? [WishlistHistory] {
                progressData = progressArray.sorted {
                    guard let initial = $0.dateData,
                          let next = $1.dateData else { return false }
                    return initial > next
                }
            }
        }
        
        guard let data = salary.first,
              let date = progressData.first?.dateData else { return }
        
        let secondInDay = 86400
        let nextMonth = date.addingTimeInterval(Double(secondInDay * 31))
        let today = Date(timeIntervalSinceNow: 0)
        if today >= nextMonth {
            switch wishlistData.count {
            case 1:
                let progressA = WishlistHistory(context: context)
                progressA.dateData = nextMonth
                progressA.progressAmount = (wishlistData[0].percentage / 100) * data.amount * (data.wishlistPercentage / 100)
                
                wishlistData[0].addToWishlistHistory(progressA)
                
                DataManager.shared.saveData()
            case 2:
                let progressA = WishlistHistory(context: context)
                progressA.dateData = nextMonth
                progressA.progressAmount = (wishlistData[0].percentage / 100) * data.amount * (data.wishlistPercentage / 100)
                
                let progressB = WishlistHistory(context: context)
                progressB.dateData = nextMonth
                progressB.progressAmount = (wishlistData[1].percentage / 100) * data.amount * (data.wishlistPercentage / 100)
                
                wishlistData[0].addToWishlistHistory(progressA)
                wishlistData[1].addToWishlistHistory(progressB)
                
                DataManager.shared.saveData()
            case 3:
                let progressA = WishlistHistory(context: context)
                progressA.dateData = nextMonth
                progressA.progressAmount = (wishlistData[0].percentage / 100) * data.amount * (data.wishlistPercentage / 100)
                
                let progressB = WishlistHistory(context: context)
                progressB.dateData = nextMonth
                progressB.progressAmount = (wishlistData[1].percentage / 100) * data.amount * (data.wishlistPercentage / 100)
                
                let progressC = WishlistHistory(context: context)
                progressC.dateData = nextMonth
                progressC.progressAmount = (wishlistData[2].percentage / 100) * data.amount * (data.wishlistPercentage / 100)
                
                wishlistData[0].addToWishlistHistory(progressA)
                wishlistData[1].addToWishlistHistory(progressB)
                wishlistData[2].addToWishlistHistory(progressC)
                
                DataManager.shared.saveData()
            default:
                print("You are boring")
            }
        }
    }
    
    @objc func gesture( gesture: UILongPressGestureRecognizer){
        tableview.setEditing(true, animated: true)
    }
    
    // MARK: Delete Wishlist
    @IBAction func deleteWishlist(_ sender: UIStoryboardSegue) {
        if let wish = self.passedWishlist {
            salary.first?.removeFromWishlist(wish)
            DataManager.shared.saveData()
        }
        
        fetchItems()
        
        if let wishlist = salary.first?.wishlist,
           let wishlistArray = wishlist.allObjects as? [Wishlist] {
            let wishlistSorted = wishlistArray.sorted {
                return $0.percentage > $1.percentage
            }
            self.wishlistData = wishlistSorted
        }
        
        let wishlistDataDict:[String: [Wishlist]] = ["wishlist": wishlistData]
        let name = Notification.Name(deleteWishlistNotificationKey)
        NotificationCenter.default.post(name: name, object: nil, userInfo: wishlistDataDict)
        
        DispatchQueue.main.async {
            self.tableview.reloadData()
        }
    }
}

// MARK: TableView
extension WishlistViewControllers {
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        self.tableView.setEditing(false, animated: true)
        return true
    }
   
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if wishlistData.isEmpty {
            return 1
        } else {
            return wishlistData.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if wishlistData.isEmpty {
            guard let cell = tableview.dequeueReusableCell(withIdentifier: "empty") else {
                fatalError()
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "wishlist", for: indexPath) as! WishlistTableViewCell
            let data = wishlistData[indexPath.row]
            var progress: Float = 0
            
            if let progressArray = data.wishlistHistory?.allObjects as? [WishlistHistory] {
                for prog in progressArray {
                    progress += prog.progressAmount
                }
            }
            
            if let total = DataFormatter.shared.formatToIDR(data.total),
               let progress = DataFormatter.shared.formatToIDR(progress) {
                cell.Total.text = "Rp. \(progress) / Rp. \(total)"
            }
            
            cell.name.text = data.name
            cell.priority.text = String((indexPath.row)+1)
            cell.progressBar.progress = progress/data.total
            cell.WishListContentView.backgroundColor = .systemBackground
            cell.WishListContentView.layer.shadowRadius = CGFloat(7.0)
            cell.WishListContentView.layer.shadowOffset = CGSize(width: 0, height: 4)
            cell.WishListContentView.layer.shadowOpacity = 0.1
            cell.WishListContentView.layer.cornerRadius = 25
            cell.PriorityContentView.layer.cornerRadius = cell.PriorityContentView.frame.height / 2
            cell.progressBar.layer.cornerRadius = 7
            return cell
        }
    }
   
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath:IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle.none
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        wishlistData.swapAt(sourceIndexPath.row, destinationIndexPath.row)
        
        DispatchQueue.main.async {
            self.tableview .reloadData()
        }
    }
}

// MARK: Data Transmission
extension WishlistViewControllers {
    static let showWishlistDetail = "showWishlistDetail"
    static let addWishlist = "addWishlist"
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case Self.showWishlistDetail:
            if let destination = segue.destination as? DetailWishlistController,
               let cell = sender as? UITableViewCell,
               let indexPath = tableView.indexPath(for: cell),
               let data = salary.first {
                let amount = (data.amount * (data.wishlistPercentage / 100))
                let detailPerMonth = (amount * (wishlistData[indexPath.row].percentage / 100))
                destination.configure(with: wishlistData[indexPath.row], and: detailPerMonth)
                destination.editWishlistDelegate = self
            }
        case Self.addWishlist:
            if let destination = segue.destination as? AddWishlistController {
                guard let data = salary.first else { fatalError("Salary data not found") }
                destination.configure(with: wishlistData, and: data)
                destination.addNewWishlist = self
            }
        default:
            print("No Segue")
        }
    }
}

// MARK: Fetch Core Data
extension WishlistViewControllers: CoreDataFetchDelegate {
    func fetchItems() {
        do {
            let request: NSFetchRequest<Salary> = Salary.fetchRequest()
            request.returnsObjectsAsFaults = false
            self.salary = try context.fetch(request)

            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } catch {
            print(error)
        }
    }
}

// MARK: Observer Notification
extension WishlistViewControllers {
    func createObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateDataObs(_:)), name: percentUpdate, object: nil)
    }
    
    @objc func updateDataObs(_ notification: NSNotification) {
        guard let dict = notification.userInfo as NSDictionary?,
              let data = dict["wishlist"] as? [Wishlist] else { fatalError("Notification info not found") }
        
        let wishlistSorted = data.sorted {
            return $0.percentage > $1.percentage
        }
        
        self.wishlistData = wishlistSorted
        
        DispatchQueue.main.async {
            self.tableview.reloadData()
        }
    }
}

// MARK: Protocol Delegate
extension WishlistViewControllers: AddNewWishlist {
    func refreshWishlist(wishlist: [Wishlist]) {
        fetchItems()
        guard let wishlist = salary.first?.wishlist else { fatalError("No wishlist data") }
        
        if let wishlistArray = wishlist.allObjects as? [Wishlist] {
            let wishlistSorted = wishlistArray.sorted {
                return $0.percentage > $1.percentage
            }
            
            self.wishlistData = wishlistSorted
        }
        
        DispatchQueue.main.async {
            self.tableview.reloadData()
        }
    }
}

extension WishlistViewControllers: EditWishlistDelegate {
    func refresh(wishlist: Wishlist) {
        for (i, wish) in wishlistData.enumerated() {
            if wishlist.id == wish.id {
                wishlistData[i] = wishlist
            }
        }
        
        DispatchQueue.main.async {
            self.tableview.reloadData()
        }
    }
}

