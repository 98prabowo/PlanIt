//
//  DetailWishlistController.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 16/06/21.
//  Maintained by Dimas Prabowo since 17/06/21.
//

import Foundation
import UIKit

protocol EditWishlistDelegate {
    func refresh(wishlist: Wishlist)
}

class DetailWishlistController: UITableViewController {
    private var progressAmount: Float?
    private var detailProgress: Float?
    private var wishlist: Wishlist?
    private var progressData = [WishlistHistory]()
    var editWishlistDelegate: EditWishlistDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let wishlist = wishlist,
              let progress = wishlist.wishlistHistory else { fatalError("No wishlist data") }
        
        if let progressArray = progress.allObjects as? [WishlistHistory] {
            let progressSorted = progressArray.sorted {
                guard let initial = $0.dateData,
                      let next = $1.dateData else { return false }
                return initial > next
            }
            self.progressData = progressSorted
        }
        
        var _progress: Float = 0
        for prog in self.progressData {
            _progress += prog.progressAmount
        }
        self.progressAmount = _progress
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let data = wishlist {
            editWishlistDelegate.refresh(wishlist: data)
        }
    }
}

// MARK: TableView
extension DetailWishlistController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let maxProgressShowed = 12
        
        if progressData.count <= 10 {
            return progressData.count + 2
        } else {
            return maxProgressShowed + 2
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "header") as? HeaderDetailWishlistCell else { fatalError("Unable to deque header cell") }
            guard let data = wishlist,
                  let name = data.name,
                  let image = data.imageData else { fatalError("Wishlist data not found") }
            
            cell.wishlistImage.image = UIImage(data: image)
            cell.wishlistImage.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 25)
            cell.wishlistName.text = name
            
            if let detail = detailProgress,
               let _progress = progressAmount,
               let _total = wishlist?.total,
               let progIDR = DataFormatter.shared.formatToIDR(_progress),
               let totalIDR = DataFormatter.shared.formatToIDR(_total) {
                cell.progressLabel.text = "Rp. \(progIDR) / Rp. \(totalIDR)"
                cell.progressBar.progress = _progress / _total
                
                var txt = ""
                let duration = (_total - _progress) / detail
                if duration.isInfinite {
                    txt = "Set your salary"
                } else if duration >= 1 {
                    txt = "\(Int(duration)) months left"
                } else {
                    let durationDays = duration * 30
                    if durationDays > 1 {
                        txt  = "\(Int(durationDays)) days left"
                    } else {
                        txt = "Congratulation!!!"
                    }
                }
                
                cell.durationLabel.text = txt
            }
            
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "subHeader") else { fatalError("Unable to deque sub header cell") }
            
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "wishlistProgress") as? ProgressWishlistCell else { fatalError("Unable to deque progress cell") }
            
            cell.progressBG.backgroundColor = .systemBackground
            cell.progressBG.layer.cornerRadius = 20
            cell.progressBG.layer.shadowRadius = CGFloat(7.0)
            cell.progressBG.layer.shadowOffset = CGSize(width: 0, height: 4)
            cell.progressBG.layer.shadowOpacity = 0.1
            cell.amountBG.backgroundColor = Colors.yellow
            cell.amountBG.layer.cornerRadius = 20
            
            if let date = progressData[indexPath.row - 2].dateData {
                cell.progressDate.text = DataFormatter.shared.formatDate().string(from: date)
            }
            
            let detail = progressData[indexPath.row - 2].progressAmount
            
            if let amountIDR = DataFormatter.shared.formatToIDR(Int(detail)) {
                cell.progressAmount.text = "Rp. \(amountIDR)"
            }
            
            return cell
        }
    }
}

// MARK: Data Transmission
extension DetailWishlistController {
    static let editWishlist = "editWishlist"
    
    func configure(with wishlist: Wishlist?, and amount: Float) {
        self.wishlist = wishlist
        self.detailProgress = amount
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Self.editWishlist,
           let destination = segue.destination as? EditWishlistController,
           let data = self.wishlist {
            destination.configure(with: data)
            destination.editDetailWishlistDelegate = self
        }
    }
}

// MARK: Protocol Delegate
extension DetailWishlistController: EditDetailWishlistDelegate {
    func refresh(wishlist: Wishlist) {
        self.wishlist = wishlist
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
