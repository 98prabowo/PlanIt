//
//  NotificationKey.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 20/06/21.
//

import Foundation

let percentNotificationKey = "com.dimasprabowo.updatePercentage"
let newWishlistNotificationKey = "com.dimasprabowo.newWishlist"
let deleteWishlistNotificationKey = "deleteWishlistNotificationKey"
