//
//  FetchItem.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 23/06/21.
//

import Foundation
import CoreData

protocol CoreDataFetchDelegate {
    func fetchItems()
}
