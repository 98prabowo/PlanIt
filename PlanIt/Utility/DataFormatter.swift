//
//  DataFormatter.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 19/06/21.
//

import Foundation
import UIKit

class DataFormatter {
    static let shared = DataFormatter()
    
    func formatToIDR(_ number: Float) -> String? {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        numberFormatter.groupingSeparator = "."
        let formattedNumber = numberFormatter.string(from: NSNumber(value: number))
        return formattedNumber
    }
    
    func formatToIDR(_ number: Int) -> String? {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        numberFormatter.groupingSeparator = "."
        let formattedNumber = numberFormatter.string(from: NSNumber(value: number))
        return formattedNumber
    }
    
    func formatDate() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        return formatter
    }
}
