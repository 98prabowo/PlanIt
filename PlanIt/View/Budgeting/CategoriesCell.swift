//
//  CategoriesCollectionViewController.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 10/06/21.
//

import UIKit

class InsideCategoriesCell: UICollectionViewCell {
    @IBOutlet weak var categoriesLabel: UILabel!
}

class MainCategoriesCell: UITableViewCell {
    @IBOutlet weak var categoriesCell: UICollectionView!
}

class DetailHeaderCell: UITableViewCell {
    @IBOutlet weak var detailImage: UIImageView!
    @IBOutlet weak var detailSpended: UILabel!
    @IBOutlet weak var detailTotal: UILabel!
}

class HistoryCell: UITableViewCell {
    @IBOutlet weak var expensesDate: UILabel!
    @IBOutlet weak var categoryExpenses: UILabel!
    @IBOutlet weak var expensesAmount: UILabel!
    @IBOutlet weak var dateBG: UIView!
    @IBOutlet weak var historyBG: UIView!
}

