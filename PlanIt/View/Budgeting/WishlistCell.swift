//
//  WishlistCell.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 14/06/21.
//

import Foundation
import UIKit

class WishlistCell: UITableViewCell {
    @IBOutlet weak var wishlistView: UIView!
    @IBOutlet weak var wishlistProgress: UILabel!
    @IBOutlet weak var wishlistImage: UIImageView!
    @IBOutlet weak var progressBar: UIProgressView!
}
