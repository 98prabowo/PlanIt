//
//  DetailWishlistCell.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 14/06/21.
//

import Foundation
import UIKit

class BudgetingWishlistHeaderCell: UITableViewCell {
    @IBOutlet weak var wishlistImage: UIImageView!
    @IBOutlet weak var progressPerMonth: UILabel!
}

class BudgetingWishlistHistoryCell: UITableViewCell {
    @IBOutlet weak var percentage: UILabel!
    @IBOutlet weak var percentageBG: UIView!
    @IBOutlet weak var wishlist: UILabel!
    @IBOutlet weak var detailPerMonth: UILabel!
    @IBOutlet weak var wishlistBG: UIView!
}
