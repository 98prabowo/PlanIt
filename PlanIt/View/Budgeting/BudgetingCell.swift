//
//  BudgetingCell.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 10/06/21.
//

import Foundation
import UIKit

class BudgetingCell: UITableViewCell {
    @IBOutlet weak var categoriesView: UIView!
    @IBOutlet weak var categoriesImage: UIImageView!
    @IBOutlet weak var categoriesLabel: UILabel!
    @IBOutlet weak var categoriesAmount: UILabel!
    @IBOutlet weak var progressBudget: UIProgressView!
}
