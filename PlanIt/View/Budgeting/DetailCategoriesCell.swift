//
//  DetailCategoriesCell.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 14/06/21.
//

import Foundation
import UIKit

class DetailCategoriesCell: UITableViewCell {
    @IBOutlet weak var categoriesLabel: UILabel!
    @IBOutlet weak var categoriesBG: UIView!
    @IBOutlet weak var deleteButton: UIButton!
}

