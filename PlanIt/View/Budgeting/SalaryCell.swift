//
//  SalaryCell.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 10/06/21.
//

import Foundation
import UIKit

class SalaryCell: UITableViewCell {
    @IBOutlet weak var salaryBG: UIView!
    @IBOutlet weak var salaryLabel: UILabel!
}
