//
//  DetailWishlistCell.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 16/06/21.
//

import UIKit

class HeaderDetailWishlistCell: UITableViewCell {
    @IBOutlet weak var wishlistImage: UIImageView!
    @IBOutlet weak var wishlistName: UILabel!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
}

class ProgressWishlistCell: UITableViewCell {
    @IBOutlet weak var progressBG: WishlistCell!
    @IBOutlet weak var amountBG: UIView!
    @IBOutlet weak var progressDate: UILabel!
    @IBOutlet weak var progressAmount: UILabel!
}
