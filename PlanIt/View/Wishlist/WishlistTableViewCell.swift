//
//  WishlistTableViewCell.swift
//  PlanIt
//
//  Created by Yanandra Dhafa on 09/06/21.
//

import UIKit

class WishlistTableViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var priority: UILabel!
    @IBOutlet weak var Total: UILabel!
    @IBOutlet weak var PriorityContentView: UIView!
    @IBOutlet weak var WishListContentView: UIView!
}
