//
//  Data.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 19/06/21.
//

import Foundation
import UIKit

class DataApps {
    let context = DataManager.shared.context
    static let shared = DataApps()
    
    func populateData() {
        // MARK: Create Salary
        let salary = Salary(context: context)
        salary.amount = 0
        salary.budgetPercentage = 80
        salary.wishlistPercentage = 20
        
        // MARK: Create Budget
        let needs = Budget(context: context)
        needs.id = 0
        needs.category = "Needs"
        needs.imageString = "image1"
        needs.imageData = UIImage(named: "image1")?.pngData()
        needs.percentage = salary.budgetPercentage - 30
        needs.total = salary.amount * needs.percentage / 100
        needs.spended = 0
        needs.detailCategories = ["New Wishlist", "Electricity", "Foods", "Drinks", "Households", "Education"]
        
        let wants = Budget(context: context)
        wants.id = 1
        wants.category = "Wants"
        wants.imageString = "image2"
        wants.imageData = UIImage(named: "image2")?.pngData()
        wants.percentage = (salary.budgetPercentage - needs.percentage) - 10
        wants.total = salary.amount * wants.percentage / 100
        wants.spended = 0
        wants.detailCategories = ["Beauty", "Apparel", "Entertainment","Music", "Movie", "Game"]
        
        let others = Budget(context: context)
        others.id = 2
        others.category = "Others"
        others.imageString = "image3"
        others.imageData = UIImage(named: "image3")?.pngData()
        others.percentage = (salary.budgetPercentage - needs.percentage - wants.percentage)
        others.total = salary.amount * others.percentage / 100
        others.spended = 0
        others.detailCategories = ["Emergency", "Unexpected"]
        
        // MARK: Create Relational Data
        salary.addToBudget(NSSet(array: [needs, wants, others]))
        
        DataManager.shared.saveData()
    }
}
