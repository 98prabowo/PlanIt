//
//  WishlistHistory+CoreDataProperties.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 16/06/21.
//
//

import Foundation
import CoreData


extension WishlistHistory {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WishlistHistory> {
        return NSFetchRequest<WishlistHistory>(entityName: "WishlistHistory")
    }

    @NSManaged public var dateData: Date?
    @NSManaged public var dateString: String?
    @NSManaged public var progressAmount: Float
    @NSManaged public var wishlist: Wishlist?

}

extension WishlistHistory : Identifiable {

}
