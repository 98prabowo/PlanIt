//
//  Budget+CoreDataProperties.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 17/06/21.
//
//

import Foundation
import CoreData


extension Budget {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Budget> {
        return NSFetchRequest<Budget>(entityName: "Budget")
    }

    @NSManaged public var category: String?
    @NSManaged public var detailCategories: [String]?
    @NSManaged public var id: Int64
    @NSManaged public var imageData: Data?
    @NSManaged public var imageString: String?
    @NSManaged public var percentage: Float
    @NSManaged public var spended: Float
    @NSManaged public var total: Float
    @NSManaged public var expenseHistory: NSSet?
    @NSManaged public var salary: Salary?

}

// MARK: Generated accessors for expenseHistory
extension Budget {

    @objc(addExpenseHistoryObject:)
    @NSManaged public func addToExpenseHistory(_ value: ExpenseHistory)

    @objc(removeExpenseHistoryObject:)
    @NSManaged public func removeFromExpenseHistory(_ value: ExpenseHistory)

    @objc(addExpenseHistory:)
    @NSManaged public func addToExpenseHistory(_ values: NSSet)

    @objc(removeExpenseHistory:)
    @NSManaged public func removeFromExpenseHistory(_ values: NSSet)

}

extension Budget : Identifiable {

}
