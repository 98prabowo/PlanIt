//
//  ExpenseHistory+CoreDataProperties.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 16/06/21.
//
//

import Foundation
import CoreData


extension ExpenseHistory {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ExpenseHistory> {
        return NSFetchRequest<ExpenseHistory>(entityName: "ExpenseHistory")
    }

    @NSManaged public var category: String?
    @NSManaged public var dateData: Date?
    @NSManaged public var dateString: String?
    @NSManaged public var detailAmount: Float
    @NSManaged public var budget: Budget?

}

extension ExpenseHistory : Identifiable {

}
