//
//  Wishlist+CoreDataProperties.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 17/06/21.
//
//

import Foundation
import CoreData


extension Wishlist {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Wishlist> {
        return NSFetchRequest<Wishlist>(entityName: "Wishlist")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var imageData: Data?
    @NSManaged public var imageString: String?
    @NSManaged public var name: String?
    @NSManaged public var percentage: Float
    @NSManaged public var progress: Float
    @NSManaged public var total: Float
    @NSManaged public var salary: Salary?
    @NSManaged public var wishlistHistory: NSSet?

}

// MARK: Generated accessors for wishlistHistory
extension Wishlist {

    @objc(addWishlistHistoryObject:)
    @NSManaged public func addToWishlistHistory(_ value: WishlistHistory)

    @objc(removeWishlistHistoryObject:)
    @NSManaged public func removeFromWishlistHistory(_ value: WishlistHistory)

    @objc(addWishlistHistory:)
    @NSManaged public func addToWishlistHistory(_ values: NSSet)

    @objc(removeWishlistHistory:)
    @NSManaged public func removeFromWishlistHistory(_ values: NSSet)

}

extension Wishlist : Identifiable {

}
