//
//  Salary+CoreDataProperties.swift
//  PlanIt
//
//  Created by Dimas A. Prabowo on 16/06/21.
//
//

import Foundation
import CoreData


extension Salary {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Salary> {
        return NSFetchRequest<Salary>(entityName: "Salary")
    }

    @NSManaged public var amount: Float
    @NSManaged public var budgetPercentage: Float
    @NSManaged public var wishlistPercentage: Float
    @NSManaged public var budget: NSSet?
    @NSManaged public var wishlist: NSSet?

}

// MARK: Generated accessors for budget
extension Salary {

    @objc(addBudgetObject:)
    @NSManaged public func addToBudget(_ value: Budget)

    @objc(removeBudgetObject:)
    @NSManaged public func removeFromBudget(_ value: Budget)

    @objc(addBudget:)
    @NSManaged public func addToBudget(_ values: NSSet)

    @objc(removeBudget:)
    @NSManaged public func removeFromBudget(_ values: NSSet)

}

// MARK: Generated accessors for wishlist
extension Salary {

    @objc(addWishlistObject:)
    @NSManaged public func addToWishlist(_ value: Wishlist)

    @objc(removeWishlistObject:)
    @NSManaged public func removeFromWishlist(_ value: Wishlist)

    @objc(addWishlist:)
    @NSManaged public func addToWishlist(_ values: NSSet)

    @objc(removeWishlist:)
    @NSManaged public func removeFromWishlist(_ values: NSSet)

}

extension Salary : Identifiable {

}
